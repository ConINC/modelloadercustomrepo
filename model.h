#ifndef model_h_
#define model_h_


typedef struct
{
    char *name;
    _LIST *tx, *ty, *tz;
    _LIST *txd, *tyd, *tzd;

    _LIST *rx, *ry, *rz;
    _LIST *rxd, *ryd, *rzd;

    _LIST *sx, *sy, *sz;
    _LIST *sxd, *syd, *szd;
}Joint_Key_List;

typedef struct
{
    char  *name;

    int t_count, td_count;
    float *tx, *ty, *tz;
    int   *txd, *tyd, *tzd;

    int r_count, rd_count;
    float *rx, *ry, *rz;
    int   *rxd, *ryd, *rzd;

    int s_count, sd_count;
    float *sx, *sy, *sz;
    int   *sxd, *syd, *szd;
}Joint_Key;

typedef struct
{
    _LIST *parents, *names;
}Connections;

typedef struct model_str
{
    char *modelname, *texturepath;
    unsigned int p_id, uv_id, n_id, w_id, wi_id, point_index_id;
    int vertex_count;
    int p_size, uv_size, n_size, w_size, wi_size, point_index_size; // count
    Texture_2D *texture_ptr, *normal_ptr, *specular_ptr;
    _LIST *skeleton;
    int skeleton_depth;

    //Animcontroller missing
}Model;


typedef struct
{
    char *name;
    float *weights;
    int *indices;
    _LIST *weightsl;
    _LIST *indicesl;
}Deformer;

typedef struct
{
    _LIST *joint_keys;
    int frame_index;
}Frame;

typedef struct
{
    _LIST *joint;
    char *animation_name;
    int animation_index;
}Animation;


//LOADING OBJ  AND SKINNING !TODAY

#endif
