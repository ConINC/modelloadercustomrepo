
#include <GLFW/glfw3.h>
#include "model.h"
#include "stdio.h"
#include "vector.h"
#include "string.h"
#include "linmath.h"

#define DEBUG_PRINT 0
#define STRING_MAX 64
#define PTR32_SIZE 4
#define MATRIX_SIZE 64
#define INT_SIZE 4
#define FLOAT_SIZE 4
#define VECTOR3_SIZE 12
#define VECTOR2_SIZE 8
#define VECTOR4_SIZE 16

///This function allocates a buffer on the gpu and returns the buffer_id
GLuint create_buffer_float(int size, float *buffer)
{
    GLuint vb_id;
    glGenBuffers(1, &vb_id);
    glBindBuffer(GL_ARRAY_BUFFER, vb_id);
    glBufferData(GL_ARRAY_BUFFER, size, buffer, GL_STATIC_DRAW);
    return vb_id;
}

Model *load_model_skinned(char *filename)
{
    filename = "C://Users//Conex//Documents//Game_Workspace//GLFW_w_GLAD//content//models//animated_test.mesh";

    //-------------Load Model alone first, then later fill in the skinning part----------------------------//
    ///Pointers:
    FILE *file;
    Model *model;
    Connections *connections;
    Deformer_List_Pack *deformer;
    Vector3D *p, *n, *new_points;
    Vector4D *weights, *indices;
    Vector2D *u, *new_uvs;
    Vector4D *w_indexed;
    Vector4D *wi_indexed;

    char *buf1, *buf2;
    int *pi, *ui, j;
    int point_count, point_index_count, normal_count, uv_count, uv_index_count;
	int ps, pis, ns ,uvs ,uvis, i,z;
    char *empty = "Empty";

    ///Load the static model now:
    model = malloc(sizeof(Model));
    if(model != NULL) puts("Model is now initialised");

	puts("Opening file:\n");
    ///"C:\\Users\\Conex\\Desktop\\Java Projekt\\test.mesh",
	file = fopen(filename, "rb");

	if(file == NULL)
		puts("Failed to open k?\n");
	else
		printf("Could open file %s\n", "lol");

	//Allocate space for the texture path and meshname strings
	buf1 = malloc(STRING_MAX);
	buf2 = malloc(STRING_MAX);

	//------------------------------------------------------------------------------//
	//read in the mesh-data-list-sizes and null terminate strings
	fread(buf1,STRING_MAX,1,file);
	fread(buf2,STRING_MAX,1,file);

	buf1[STRING_MAX-1] = '\0';
	buf2[STRING_MAX-1] = '\0';
	puts(buf1);
    puts(buf2);

	fread(&ps, FLOAT_SIZE,1, file);
	fread(&pis, FLOAT_SIZE,1, file);
	fread(&ns, FLOAT_SIZE,1, file);
	fread(&uvs, FLOAT_SIZE,1, file);
	fread(&uvis, FLOAT_SIZE,1, file);
	//------------------------------------------------------------------------------//

	//------------------------------------------------------------------------------//
	//Allocate space for the mesh data
	p = (Vector3D*)malloc(ps);
	pi = (int*)malloc(pis);
	n = (Vector3D*)malloc(ns);
	u = (Vector2D*)malloc(uvs);
	ui = (int*)malloc(uvis);
    //------------------------------------------------------------------------------//

	//------------------------------------------------------------------------------//
	//read in the whole lists
	fread(p, ps, 1, file);
	fread(pi,pis, 1, file);
	fread(n, ns, 1, file);
	fread(u, uvs,1, file);
	fread(ui, uvis, 1, file);
	//------------------------------------------------------------------------------//

    fclose(file);

	//------------------------------------------------------------------------------//
	//print the values, can be turned off
	if(1)
	{
		for(i = 0; i < ps / VECTOR3_SIZE; i++)
			printf("P: %f %f %f i=%d___", p[i].x, p[i].y, p[i].z, i);

		for(i = 0; i < ns / VECTOR3_SIZE; i++)
			printf("N: %f %f %f i=%d___",n[i].x, n[i].y, n[i].z, i);

		for(i = 0; i < uvs*0/ VECTOR2_SIZE; i++)
			printf("UV: %f %f i=%d___", u[i].x, u[i].y, i);


        for(i = 0; i < pis*0/4; i+=3)
			printf("Point Index: %d %d %d at i=%d\n", pi[i],pi[i+1],pi[i+2], i);

    }
	//------------------------------------------------------------------------------//


	printf("\n%s\n%s", buf1, buf2);
	printf("\n%d %d %d %d %d\n", ps/FLOAT_SIZE,pis/FLOAT_SIZE,ns/FLOAT_SIZE,uvs/FLOAT_SIZE,uvis/FLOAT_SIZE);
	printf("Loading file %s succesful\n", filename);

    //Important step, we have to run the index of UV and Points
    //throught the actual lists of points and uvs, but in tripple-floa
    //steps, since the indices work on points, not single floats
    point_count = ps / VECTOR3_SIZE;
    point_index_count = pis /FLOAT_SIZE;
    normal_count = ns / VECTOR3_SIZE;
    uv_count = uvs / VECTOR2_SIZE;
    uv_index_count = uvis / FLOAT_SIZE;

    //Create temp lists , from indexed to raw list
    new_points = malloc(VECTOR3_SIZE*point_index_count);
    new_uvs = malloc(VECTOR2_SIZE * uv_index_count);

    for(i = 0, z = 0; i < point_index_count; i++, z++)
    {
        new_points[i] = p[pi[i]];
    }

    for(i = 0 , z = 0; i < uv_index_count; i++, z++)
    {
        new_uvs[i] = u[ui[i]];
    }

    model->texturepath = buf2;
    model->modelname = buf1;
    model->p_size = VECTOR3_SIZE * point_index_count;
    model->w_size = VECTOR4_SIZE * point_index_count;
    model->wi_size = VECTOR4_SIZE * point_index_count;
    model->n_size= ns;
    model->uv_size= VECTOR2_SIZE * uv_index_count;

    //-----------------------------------------------------------------------------------------------------//
    // Now load the other stuff ontop
    //-----------------------------------------------------------------------------------------------------//
    deformer = load_model_deformers("C://Users//Conex//Documents//Game_Workspace//GLFW_w_GLAD//content//models//animated_test.deformer");
    connections = load_connections("C://Users//Conex//Documents//Game_Workspace//GLFW_w_GLAD//content//models//animated_test.connection");

    //Now we need to create a vertex format format as following
    // point, uv, normal, weights[4], indices[4]
    printf("P-size is now : %d\n",point_count);

    weights = malloc(sizeof(Vector4D) * point_count);
    indices = malloc(sizeof(Vector4D) * point_count);

    for(i = 0; i < point_count; i++)
    {
        weights[i].x = 0; weights[i].y = 0; weights[i].z = 0; weights[i].w = 0;
        indices[i].x = -1; indices[i].y = -1; indices[i].z = -1; indices[i].w = -1;
    }

    //At this point, the weights are refering to the indexed points, we will fix it by
    //also assignign the weights to a full-triangulated list of points using the indices
    printf("Point count: %d\n", point_count);
    printf("Deformer count: %d\n", deformer->deformer_count);
    for(i = 0; i < deformer->deformer_count; i++) // for joint
    {
        printf("Index count: %d\n\n", deformer->deformers[i]->indices_count);
        for(j = 0; j < deformer->deformers[i]->indices_count; j++) //for weight/index
        {
            if( weights[deformer->deformers[i]->indices[j]].x < 0.01)
            {
                weights[deformer->deformers[i]->indices[j]].x = deformer->deformers[i]->weights[j];
                indices[deformer->deformers[i]->indices[j]].x = i;

                printf("%f \n", weights[deformer->deformers[i]->indices[j]].x);
                printf("%f \n", indices[deformer->deformers[i]->indices[j]].x);
            }
            else if(weights[deformer->deformers[i]->indices[j]].y < 0.01)
            {
                weights[deformer->deformers[i]->indices[j]].y = deformer->deformers[i]->weights[j];
                indices[deformer->deformers[i]->indices[j]].y = i;

                printf("%f \n", weights[deformer->deformers[i]->indices[j]].y);
                printf("%f \n", indices[deformer->deformers[i]->indices[j]].y);
            }
            else if(weights[deformer->deformers[i]->indices[j]].z < 0.01 )
            {
                weights[deformer->deformers[i]->indices[j]].z = deformer->deformers[i]->weights[j];
                indices[deformer->deformers[i]->indices[j]].z = i;

                printf("%f \n", weights[deformer->deformers[i]->indices[j]].z);
                printf("%f \n", indices[deformer->deformers[i]->indices[j]].z);
            }
            else if(weights[deformer->deformers[i]->indices[j]].w < 0.01)
            {
                weights[deformer->deformers[i]->indices[j]].w = deformer->deformers[i]->weights[j];
                indices[deformer->deformers[i]->indices[j]].w = i;

                printf("%f \n", weights[deformer->deformers[i]->indices[j]].w);
                printf("%f \n", indices[deformer->deformers[i]->indices[j]].w);
            }
        }
    }

    w_indexed = malloc(model->w_size);
    wi_indexed = malloc(model->wi_size);

    for(i = 0, z = 0; i < point_index_count; i++)
    {

        w_indexed[i].x = weights[pi[i]].x;
        w_indexed[i].y = weights[pi[i]].y;
        w_indexed[i].z = weights[pi[i]].z;
        w_indexed[i].w = weights[pi[i]].w;

        wi_indexed[i].x = indices[pi[i]].x;
        wi_indexed[i].y = indices[pi[i]].y;
        wi_indexed[i].z = indices[pi[i]].z;
        wi_indexed[i].w = indices[pi[i]].w;

        printf("%.2f %.2f %.2f %.2f %.2f %.2f %.2f %.2f \n",
            w_indexed[i].x,
            w_indexed[i].y,
            w_indexed[i].z,
            w_indexed[i].w,
            wi_indexed[i].x,
            wi_indexed[i].y,
            wi_indexed[i].z,
            wi_indexed[i].w);
    }

    // HOW TO FIX... First read in the data in the weight/indices style to a list of the same size as the point count "DONE"
    //Now, index the data using the point indices
    printf("index and weight count %d index count %d\n", point_count, pis);

    printf("DA SIZE IS: %d\n", model->p_size/12);
    model->p_id = create_buffer_float(model->p_size, (float*)new_points);
    model->uv_id = create_buffer_float(model->uv_size,(float*)new_uvs);
    model->n_id = create_buffer_float(model->n_size, n);
    model->w_id = create_buffer_float(model->w_size, (float*) w_indexed);
    model->wi_id = create_buffer_float(model->wi_size, (float*)wi_indexed);
    model->vertex_count = model->p_size / VECTOR3_SIZE;
    model->connections = connections;
    model->joints = malloc(sizeof(Joint*)*deformer->deformer_count);
    model->joint_count = deformer->deformer_count;

    printf("In connections now\n");
    for(i = 0; i < deformer->deformer_count; i++)
    {
        model->joints[i] = malloc(sizeof(Joint));
        model->joints[i]->index = i;
        model->joints[i]->matrix = malloc(sizeof(float) * 16);
        mat4x4_identity(model->joints[i]->matrix);
        model->joints[i]->name = deformer->deformers[i]->name;
         model->joints[j]->parent_name = empty;
        printf("\nParent: %s Child: %s i = %d\n",  deformer->deformers[i]->parent_name,model->joints[i]->name,model->joints[i]->index);
    }

    //Now we just need to set the correct parent names for each deformer, using the connections...
    for(i = 0; i < model->connections->count; i++)
    {
        for(j = 0; j < model->joint_count; j++)
        {
            model->joints[i]->index = i;
            mat4x4_identity(model->joints[i]->matrix);
            if( strcmp( model->joints[j]->name, model->connections->parent[i]) == 0)
            {
                model->joints[j]->parent_name = model->connections->child[i];
                printf("Joint %d: child: %s  parent: %s\n", j, model->joints[j]->name,model->joints[j]->parent_name);
            }
        }
    }

    // FREE DEFORMERS deformer->deformer[]
    for(i = 0; i < deformer->deformer_count; i++)
    {
        free(deformer->deformers[i]->indices);
        free(deformer->deformers[i]->weights);
        free(deformer->deformers[i]);
    }
    free(deformer);

    puts("\n\n\n");
    free(weights); free(new_points);
    free(indices); free(new_uvs);
    free(w_indexed);
    free(wi_indexed);

    printf("Point buffer id %d and size %d\n", model->p_id, model->p_size);
    printf("Deformer Count: %d\n", deformer->deformer_count);

    return model;
}

///Function to load .mesh files
int load_model_pose(Model** model,char *filename)
{
	FILE *file = NULL;
	int count = 0, i;

	file = fopen(filename, "rb");

	if(file == NULL)
	{
        printf("Failed to open %d\n", "C:\\Users\\Conex\\Desktop\\Java Projekt\\test.pose");
        return -1;
    }
    else
		printf("Opening file %s\n", "C:\\Users\\Conex\\Desktop\\Java Projekt\\test.pose");

	//------------------------------------------------------------------------------//
	//read in the mesh-data-list-sizes and null terminate strings
	fread(&count,4,1,file);
    printf("\n%d is Pose count\n", count);

    Pose** poses = (Pose**)malloc(4*count);
    for(i =0; i < count; i++)
    {
        poses[i] = malloc(sizeof(Pose));
        poses[i]->name = malloc(STRING_MAX);
        poses[i]->matrix = malloc(MATRIX_SIZE);
        fread(poses[i]->name, STRING_MAX,1,file);
        fread(poses[i]->matrix, STRING_MAX,1,file);
        printf("\n%s", poses[i]->name);
    }


    return 0;
}

///Function to load .mesh files
Model* load_model_static(char *filename)
{
    ///Pointers:
    FILE *file;
    Model *model;
    Vector3D *p, *n, *new_points;
    Vector2D *u, *new_uvs;
    char *buf1, *buf2;
    int *pi, *ui;

    ///Variables
    int point_count, point_index_count, normal_count, uv_count, uv_index_count;
	int ps, pis, ns ,uvs ,uvis, i,z;


    ///Load the static model now:
    model = malloc(sizeof(Model));
    if(model != NULL) puts("Model is now initialised");

	puts("Opening file:\n");
    ///"C:\\Users\\Conex\\Desktop\\Java Projekt\\test.mesh",
	file = fopen(filename, "rb");

	if(file == NULL)
		puts("Failed to open k?\n");
	else
		printf("Could open file %s\n", "lol");

	//Allocate space for the texture path and meshname strings
	buf1 = malloc(STRING_MAX);
	buf2 = malloc(STRING_MAX);

	//------------------------------------------------------------------------------//
	//read in the mesh-data-list-sizes and null terminate strings
	fread(buf1,STRING_MAX,1,file);
	fread(buf2,STRING_MAX,1,file);

	buf1[STRING_MAX-1] = '\0';
	buf2[STRING_MAX-1] = '\0';
	puts(buf1);
    puts(buf2);

	fread(&ps, FLOAT_SIZE,1, file);
	fread(&pis, FLOAT_SIZE,1, file);
	fread(&ns, FLOAT_SIZE,1, file);
	fread(&uvs, FLOAT_SIZE,1, file);
	fread(&uvis, FLOAT_SIZE,1, file);
	//------------------------------------------------------------------------------//

	//------------------------------------------------------------------------------//
	//Allocate space for the mesh data
	p = (Vector3D*)malloc(ps);
	pi = (int*)malloc(pis);
	n = (Vector3D*)malloc(ns);
	u = (Vector2D*)malloc(uvs);
	ui = (int*)malloc(uvis);
    //------------------------------------------------------------------------------//

	//------------------------------------------------------------------------------//
	//read in the whole lists
	fread(p, ps, 1, file);
	fread(pi,pis, 1, file);
	fread(n, ns, 1, file);
	fread(u, uvs,1, file);
	fread(ui, uvis, 1, file);
	//------------------------------------------------------------------------------//

	//------------------------------------------------------------------------------//
	//print the values, can be turned off
	if(1)
	{
		for(i = 0; i < ps/12; i++)
			printf("P: %f %f %f i=%d\n", p[i].x, p[i].y, p[i].z, i);

		for(i = 0; i < ns/12; i++)
			printf("N: %f %f %f i=%d\n",n[i].x, n[i].y, n[i].z, i);

		for(i = 0; i < uvs/8; i++)
			printf("UV: %f %f i=%d\n", u[i].x, u[i].y, i);

        for(i = 0; i < pis/4; i+=3)
			printf("Point Index: %d %d %d  at i=%d\n", pi[i],pi[i+1],pi[i+2], i);

    }
	//------------------------------------------------------------------------------//


	printf("\n%s\n%s", buf1, buf2);
	printf("\n%d %d %d %d %d\n", ps/FLOAT_SIZE,pis/FLOAT_SIZE,ns/FLOAT_SIZE,uvs/FLOAT_SIZE,uvis/FLOAT_SIZE);
	printf("Loading file %s succesful\n", filename);

    //Important step, we have to run the index of UV and Points
    //throught the actual lists of points and uvs, but in tripple-floa
    //steps, since the indices work on points, not single floats
    point_count = ps / VECTOR3_SIZE;
    point_index_count = pis /FLOAT_SIZE;
    normal_count = ns / VECTOR3_SIZE;
    uv_count = uvs / VECTOR2_SIZE;
    uv_index_count = uvis / FLOAT_SIZE;

    //Create temp lists , from indexed to raw list
    new_points = malloc(VECTOR3_SIZE*point_index_count);
    new_uvs = malloc(VECTOR2_SIZE * uv_index_count);


    for(i = 0, z = 0; i < point_index_count; i++, z++)
    {
        new_points[i] = p[pi[i]];
    }

    for(i = 0, z = 0; i < uv_index_count; i++, z++)
    {
        new_uvs[i] = u[ui[i]];
    }


    model->texturepath = buf2;
    model->modelname = buf1;
    model->p_size = VECTOR3_SIZE * point_index_count;
    model->n_size= ns;
    model->uv_size= VECTOR2_SIZE * uv_index_count;

    model->p_id = create_buffer_float(model->p_size, (float*)new_points);
    model->uv_id = create_buffer_float(model->uv_size,(float*)new_uvs);
    model->n_id = create_buffer_float(model->n_size, n);
    model->vertex_count = model->p_size / VECTOR3_SIZE;

    puts("\nLOAD STATIC\n\n");
    for(i = 0, z = 0; i < point_index_count; i++, z++)
    {
        printf("Point: %f %f %f\n", new_points[i].x, new_points[i].y, new_points[i].z);
    }
    // FREE DEFORMERS deformer->deformer[]
    puts("\n\n\n");

    model->non_indexed_p_count = point_count;


    free(p);  free(n); free(new_points);
	free(u);  free(new_uvs);
	free(pi);  free(ui);

    fclose(file);

    print_model(model);
    puts("Succesfully loaded model\n");
    return model;
}

///Function to load .deformer files
Deformer_List_Pack* load_model_deformers(char *filename)
{
    puts("Reading deformers now\n\n");
    int deformer_count = 0, i = 0;
    FILE *in;

    printf("Reading Binary file with path\n%s now!\n", filename);

    ///"C:\\Users\\Conex\\Desktop\\Java Projekt\\test.deformer"
    in = fopen(filename, "rb");

    fread(&deformer_count, sizeof(int),1, in);
    printf("Deformer Count: %d\n", deformer_count);

    Deformer **deformers = malloc(sizeof(Deformer*)*deformer_count);

    for(i = 0; i < deformer_count; i++)
    {
        puts("\n-------------------------------------\n");
        Deformer *d = malloc(sizeof(Deformer));
        d->name = malloc(64);
        fread(d->name, 64,1,in);
        fread(&d->weight_count, 4,1,in);
        fread(&d->indices_count, 4, 1,in);
        d->weights = malloc(d->weight_count*4);
        d->indices = malloc(d->indices_count*4);

        fread(d->weights,d->weight_count*4, 1,in);
        fread(d->indices,d->indices_count*4, 1,in);

        printf("Name: %s\nw_count: %d \ni_count: %d", d->name, d->weight_count,d->indices_count);
        int x;
        puts("\nNOW WEIGHTS\n");
        for(x = 0; x < d->weight_count; x++)
            printf("%f", d->weights[x]);
        puts("\nNOW INDICES\n");
        for(x = 0; x < d->indices_count ; x++)
            printf(" %d", d->indices[x]);

        deformers[i] = d;
    }

    fclose(in);

    Deformer_List_Pack *pack = malloc(sizeof(Deformer_List_Pack));
    pack->deformers = deformers;
    pack->deformer_count = deformer_count;

    return pack;
}

Connections *load_connections(char *filename)
{
    Connections *con_ptr;
    FILE *in;
    int i,connection_count;
    char **parent, **child;

    printf("Reading Binary file with path\n%s now!\n", filename);
    /// "C:\\Users\\Conex\\Desktop\\Java Projekt\\test.connection"
    in = fopen(filename, "rb");

    fread(&connection_count, sizeof(int), 1, in);

    parent = (char**)malloc(sizeof(char*)*connection_count);
    child = (char**)malloc(sizeof(char*)*connection_count);

    for(i = 0; i < connection_count; i++)
    {
        parent[i] = (char*)malloc(64);
        child[i] = (char*)malloc(64);

        fread(parent[i], 64, 1, in);
        fread(child[i], 64, 1, in);
        printf("P: %s C: %s\n", parent[i], child[i]);
    }

    con_ptr = malloc(sizeof(Connections));
    con_ptr->child = child;
    con_ptr->parent= parent;
    con_ptr->count = connection_count;
    fclose(in);

    return con_ptr;
}

Animation** load_animation(char *filename)
{
    FILE *in = NULL;
    Animated_Joint **animation;
    int i,j, joint_count, d;
    char *joint_name;

    int trans_count, rotation_count, scale_count;

    int *trans_keys, *rot_keys, *scale_keys;

    float *translate_x, *translate_y, *translate_z;
    float *rot_x, *rot_y, *rot_z;
    float *scale_x, *scale_y, *scale_z;

    printf("Reading Animation file with path\n now!\n");
    ///"C:\\Users\\Conex\\Desktop\\Java Projekt\\test.animation"
    in = fopen(filename, "rb");

    if(in == NULL)
    {
        puts("Failed to open file\n");
        return -1;
    }

    fread(&joint_count, sizeof(int), 1, in);
    animation =  malloc(sizeof(Animated_Joint*)*joint_count);

    printf("Read in %d joints\n", joint_count);

    for(i = 0; i < joint_count; i++)
    {
        animation[i] = malloc(sizeof(Animated_Joint));
    }

    for(i = 0; i < joint_count; i++)
    {
        // FOR TRANSLATION
        //----------------------------------------------------------//
        //Get count of the following key/value pairs
        joint_name = malloc(64);

        fread(joint_name, 64,1,in);

        fread(&trans_count, sizeof(int),1,in);

        //Allocate the size we need for the data
        trans_keys = malloc(sizeof(int)*trans_count);
        translate_x = malloc(sizeof(float)*trans_count);
        translate_y = malloc(sizeof(float)*trans_count);
        translate_z = malloc(sizeof(float)*trans_count);

        //read in one key list, and the 3 value lists
        fread(trans_keys, trans_count*4, 1, in);
        fread(translate_x, trans_count*4, 1, in);
        fread(translate_y, trans_count*4, 1, in);
        fread(translate_z, trans_count*4, 1, in);
        for(d = 0; d < trans_count; d++)
        {
            printf("trans x =%f y=%f z=%f at key = %d\n", translate_x[d],translate_y[d],translate_z[d],trans_keys[d]);
        }
        //----------------------------------------------------------//


        // FOR ROTATION
        //----------------------------------------------------------//
        //Get the size of the keys/values to come
        fread(&rotation_count, 4,1,in);

        //Allocate the size we need for the data
        rot_keys = malloc(sizeof(int)*rotation_count);
        rot_x = malloc(sizeof(float)*rotation_count);
        rot_y = malloc(sizeof(float)*rotation_count);
        rot_z = malloc(sizeof(float)*rotation_count);

        //read in one key list, and the 3 value lists
        fread(rot_keys, rotation_count*4, 1, in);
        fread(rot_x, rotation_count*4, 1, in);
        fread(rot_y, rotation_count*4, 1, in);
        fread(rot_z, rotation_count*4, 1, in);
        for(d = 0; d < rotation_count; d++)
        {
            printf("rot x =%f y=%f z=%f at key = %d\n", rot_x[d], rot_y[d],rot_z[d],rot_keys[d]);
        }
        //----------------------------------------------------------//


        // FOR SCALE
        //----------------------------------------------------------//
        //Get the size of the keys/values to come
        fread(&scale_count, 4,1,in);

        //Allocate the size we need for the data
        scale_keys = malloc(sizeof(int)*scale_count);
        scale_x = malloc(sizeof(float)*scale_count);
        scale_y = malloc(sizeof(float)*scale_count);
        scale_z = malloc(sizeof(float)*scale_count);

        //read in one key list, and the 3 value lists
        fread(scale_keys, scale_count*4, 1, in);
        fread(scale_x, scale_count*4, 1, in);
        fread(scale_y, scale_count*4, 1, in);
        fread(scale_z, scale_count*4, 1, in);
        for(d = 0; d < scale_count; d++)
        {
            printf("scale x =%f y=%f z=%f at key = %d\n", scale_x[d],scale_y[d],scale_z[d],scale_keys[d]);
        }
        //----------------------------------------------------------//

        animation[i]->joint_name = joint_name;

        animation[i]->trans_count = trans_count;
        animation[i]->rot_count = rotation_count;
        animation[i]->scale_count = scale_count;

        animation[i]->trans_keys = trans_keys;
        animation[i]->rotate_keys = rot_keys;
        animation[i]->scale_keys = scale_keys;

        animation[i]->translate_x = translate_x;
        animation[i]->translate_y = translate_y;
        animation[i]->translate_z = translate_z;

        animation[i]->rotate_x = rot_x;
        animation[i]->rotate_y = rot_y;
        animation[i]->rotate_z = rot_z;

        animation[i]->scale_x = scale_x;
        animation[i]->scale_y = scale_y;
        animation[i]->scale_z = scale_z;

        printf("Joint %d\ntrans %d\nrot %d\nscale %d\n", i, trans_count, rotation_count, scale_count);
    }

    for(i = 0; i < joint_count; i++)
    {

    }
    fclose(in);
    puts("Succesfully read Animation file");

    return animation;
}

void print_model(Model* model)
{
    printf("Model:\n");
    printf("-----------------------------------------------------\n");
    printf("    Name: %s\n", model->modelname);
    printf("    Texture: %s\n", model->texturepath);
    printf("    TextureIndex: %x\n", model->texture_ptr);
    printf("    P_ID, UV_ID, N_ID: %d, %d, %d\n", model->p_id,model->uv_id,model->n_id);
    printf("    P_Size, UV_Size, N_Size: %d, %d, %d\n", model->p_size,model->uv_size,model->n_size);
    printf("    TriangleCount: %d\n", model->vertex_count);
    printf("-----------------------------------------------------\n\n");
    fflush(stdout);
}


