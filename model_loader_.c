#include "model.h"
#include "model_loader.h"
#include "basic_loader.h"
#include "collection.h"
#include "vector.h"

_LIST *load_deformers(char *filename)
{
    printf("Loading Deformers: Path: %S\n", filename);
    Deformer* deformer_temp;
    _LIST *deformers, *lines;
    char *buf;

    deformers       = list_new();
    buf             = load_text(filename);
    lines           = split_buffer(buf, "\n");
    int i;

    int read_weights = 0;
    int read_indices = 0;
    for(i = 0;i < list_count(lines); i++)
    {
        if(strstr(list_at(lines,i), "<DEFORMER>"))
        {
            printf("---------------------------------\n");
            deformer_temp   = malloc(sizeof(Deformer));
            deformer_temp->weightsl = list_new();
            deformer_temp->indicesl = list_new();
            continue;
        }

        if(strstr(list_at(lines,i), "NAME:"))
        {
            int len         = strlen(list_at(lines, i))+sizeof(char);
            char *line      = malloc(len);
            strcpy(line,    list_at(lines, i));

            line[len-1] = '\0';
            char *trash    = strtok(line," ");
            char *name      =  strtok(NULL, " ");

            printf("Name is: %s\n", name);
            deformer_temp->name = name;
            continue;
        }
        if(strstr(list_at(lines,i), "<WEIGHT>"))
        {
            read_weights = 1;
            continue;
        }
        if(strstr(list_at(lines,i), "</WEIGHT>"))
        {
            read_weights = 0;
            continue;
        }

        if(strstr(list_at(lines,i), "<INDEX>"))
        {
            read_indices = 1;
            continue;
        }
        if(strstr(list_at(lines,i), "</INDEX>"))
        {
            read_indices = 0;
            continue;
        }

        if(read_weights)
        {
            char *v = list_at(lines,i);
            float *val  = malloc(sizeof(float));
            (*val) = atof(v);
            list_add(deformer_temp->weightsl, val);
            printf("Weight: %f\n", *val);
            continue;
        }
        if(read_indices)
        {
            char *v = list_at(lines,i);
            int *val  = malloc(sizeof(int));
            (*val) = atoi(v);
            list_add(deformer_temp->indicesl, val);
            printf("Index: %d\n", *val);
            continue;
        }

        if(strstr(list_at(lines,i), "</DEFORMER>"))
        {
            list_add(deformers, deformer_temp);
            continue;
        }
    }
    printf("---------------------------------\n");
    return deformers;
}

Model *load_model_static(char *filename)
{
    Model *m;
    _LIST *lines, *p, *pi, *n, *uv, *uvi;
    char *buf;
    int i,j, read_p, read_pi,read_uv, read_uvi, read_n;

    read_p      = 0;
    read_pi     = 0;
    read_uv     = 0;
    read_uvi    = 0;
    read_n      = 0;

    m       = malloc(sizeof(Model));
    buf     = load_text(filename);
    lines   = split_buffer(buf, "\n");
    p       = list_new();
    pi      = list_new();
    n       = list_new();
    uv      = list_new();
    uvi     = list_new();

    for(i = 0; i < list_count(lines); i++)
    {
        printf("%s\n", list_at(lines,i));
    }

    for(i = 0; i < list_count(lines); i++)
    {
        if(strstr(list_at(lines, i), "meshname: "))
        {
            int len             = strlen(list_at(lines, i))+sizeof(char);
            char *modelname     = malloc(len);
            strcpy(modelname,   list_at(lines, i));
            modelname[len-1]    = '\0';
            char *ptr           = strtok(modelname," ");
            char *ptr2          = strtok(NULL, " ");
            m->modelname        = ptr2;
        }

        if(strstr(list_at(lines, i), "texturename: "))
        {
            int len             = strlen(list_at(lines, i))+sizeof(char);
            char *texturename   = malloc(len);
            strcpy(texturename, list_at(lines, i));
            texturename[len-1]  = '\0';
            char *ptr           = strtok(texturename," ");
            char *ptr2          = strtok(NULL, " ");
            m->texturepath      = ptr2;
        }

        //Switches to turn on/off reading of various vertex-data
        if(strstr(list_at(lines, i), "<POINTS>")) {read_p    = 1; continue;}
        if(strstr(list_at(lines, i), "</POINTS>")){ read_p   = 0; continue;}
        if(strstr(list_at(lines, i), "<POINTI>")){ read_pi   = 1; continue;}
        if(strstr(list_at(lines, i), "</POINTI>")){ read_pi  = 0; continue;}
        if(strstr(list_at(lines, i), "<NORMAL>")){ read_n    = 1; continue;}
        if(strstr(list_at(lines, i), "</NORMAL>")){ read_n   = 0; continue; }
        if(strstr(list_at(lines, i), "<UV>")){ read_uv       = 1; continue;}
        if(strstr(list_at(lines, i), "</UV>")){ read_uv      = 0; continue;}

        if(strstr(list_at(lines, i), "<UVI>"))
        {
            read_uvi        = 1;
            continue;
        }
        if(strstr(list_at(lines, i), "</UVI>"))
        {
            read_uvi        = 0;
            continue;
        }
        if(read_p)
        {
            list_add(p, list_at(lines, i));
            printf("Adding p : %s\n", list_at(lines, i));
        }
        if(read_pi)
        {
            list_add(pi, list_at(lines, i));
        }
        if(read_n)
        {
            list_add(n, list_at(lines, i));
        }
        if(read_uv)
        {
            list_add(uv, list_at(lines, i));
        }
        if(read_uvi)
        {
            list_add(uvi, list_at(lines, i));
        }
    }

    for(i = 0; i < list_count(p); i++)
    {
        printf("RePrint p: %s\n", list_at(p,i));
    }

    int l_count_p       = list_count(p) / 3;
    int l_count_pi      = list_count(pi);
    int l_count_n       = list_count(n) / 3;
    int l_count_uv      = list_count(uv) / 2;
    int l_count_uvi     = list_count(uvi);

    int *pi_dat         = malloc(sizeof(int) * l_count_pi);
    int *uvi_dat        = malloc(sizeof(int) * l_count_uvi);
    VEC3 *p_dat     = malloc(sizeof(VEC3) * l_count_p);
    VEC3 *n_dat     = malloc(sizeof(VEC3) * l_count_n);
    VEC3 *uv_dat    = malloc(sizeof(VEC3) *l_count_uv);

    VEC3 *raw_p     = malloc(sizeof(VEC3) * l_count_pi);
    VEC3 *raw_uv    = malloc(sizeof(VEC3) * l_count_uvi);

    //Points
    for(i = 0, j=0; i < l_count_p; i++, j+=3)
    {
        p_dat[i].x      = atof(list_at(p,j+0));
        p_dat[i].y      = atof(list_at(p,j+1));
        p_dat[i].z      = atof(list_at(p,j+2));

        free(list_at(p, j+0));
        free(list_at(p, j+1));
        free(list_at(p, j+2));

        printf("Points: %f %f %f\n",  p_dat[i].x, p_dat[i].y, p_dat[i].z);
    }

    //point index
    for(i = 0; i < l_count_pi; i++)
    {
        pi_dat[i]       = atoi(list_at(pi,i));
        free(list_at(pi,i));
    }

    //normals
    for(i = 0, j=0; i < l_count_n; i++, j+=3)
    {
        int index       = i*3;
        n_dat[i].x      = atof(list_at(n,j+0));
        n_dat[i].y      = atof(list_at(n,j+1));
        n_dat[i].z      = atof(list_at(n,j+2));
        free(list_at(n, j+0));
        free(list_at(n, j+1));
        free(list_at(n, j+2));

        printf("Normals: %f %f %f\n",  n_dat[i].x,n_dat[i].y,n_dat[i].z);
    }
    //uv
    for(i = 0, j = 0; i < l_count_uv; i++, j+=2)
    {
        uv_dat[i].x         = atof(list_at(uv,j+0));
        uv_dat[i].y         = atof(list_at(uv,j+1));
        free(list_at(uv,    j+0));
        free(list_at(uv,    j+1));

        printf("UV: %f %f\n",  uv_dat[i].x,uv_dat[i].y);
    }
    //point index
    for(i = 0; i < l_count_uvi; i++)
    {
        uvi_dat[i]      = atoi(list_at(uvi,i));
        free(list_at(uvi,   i));
    }

    for(i = 0; i < l_count_pi; i++)
    {
        raw_p[i].x      = p_dat[pi_dat[i]].x;
        raw_p[i].y      = p_dat[pi_dat[i]].y;
        raw_p[i].z      = p_dat[pi_dat[i]].z;

        printf("P_raw: %f %f %f\n", raw_p[i].x, raw_p[i].y, raw_p[i].z);
    }
    for(i = 0; i < l_count_uvi; i++)
    {
        raw_uv[i].x     = uv_dat[uvi_dat[i]].x;
        raw_uv[i].y     = uv_dat[uvi_dat[i]].y;

        printf("UV_raw: %f %f\n", raw_uv[i].x,raw_uv[i].y);
    }

    m->p_size           = sizeof(VEC3) * l_count_pi;
    m->uv_size          = sizeof(VEC3) * l_count_uvi;
    m->n_size           = sizeof(VEC3) * l_count_n;
    m->p_id             = create_buffer_float(m->p_size, (float*)raw_p);
    m->uv_id            = create_buffer_float(m->uv_size,(float*) raw_uv);
    m->n_id             = create_buffer_float(m->n_size, (float*) n_dat);

    free(raw_p);
    free(raw_uv);
    free(n_dat);
    m->vertex_count = l_count_pi;
    return m;
}

Model *load_model_dynamic(char *filename, char *deformer_name)
{
    _LIST *deformers = load_deformers(deformer_name);
    Model *m;
    _LIST *lines, *p, *pi, *n, *uv, *uvi;
    char *buf;
    int i,j, read_p, read_pi,read_uv, read_uvi, read_n;

    read_p      = 0;
    read_pi     = 0;
    read_uv     = 0;
    read_uvi    = 0;
    read_n      = 0;

    m       = malloc(sizeof(Model));
    buf     = load_text(filename);
    lines   = split_buffer(buf, "\n");
    p       = list_new();
    pi      = list_new();
    n       = list_new();
    uv      = list_new();
    uvi     = list_new();

    for(i = 0; i < list_count(lines); i++)
    {
        printf("%s\n", list_at(lines,i));
    }

    for(i = 0; i < list_count(lines); i++)
    {
        if(strstr(list_at(lines, i), "meshname: "))
        {
            int len             = strlen(list_at(lines, i))+sizeof(char);
            char *modelname     = malloc(len);
            strcpy(modelname,   list_at(lines, i));
            modelname[len-1]    = '\0';
            char *ptr           = strtok(modelname," ");
            char *ptr2          = strtok(NULL, " ");
            m->modelname        = ptr2;
        }

        if(strstr(list_at(lines, i), "texturename: "))
        {
            int len             = strlen(list_at(lines, i))+sizeof(char);
            char *texturename   = malloc(len);
            strcpy(texturename, list_at(lines, i));
            texturename[len-1]  = '\0';
            char *ptr           = strtok(texturename," ");
            char *ptr2          = strtok(NULL, " ");
            m->texturepath      = ptr2;
        }

        //Switches to turn on/off reading of various vertex-data
        if(strstr(list_at(lines, i), "<POINTS>")) {read_p    = 1; continue;}
        if(strstr(list_at(lines, i), "</POINTS>")){ read_p   = 0; continue;}
        if(strstr(list_at(lines, i), "<POINTI>")){ read_pi   = 1; continue;}
        if(strstr(list_at(lines, i), "</POINTI>")){ read_pi  = 0; continue;}
        if(strstr(list_at(lines, i), "<NORMAL>")){ read_n    = 1; continue;}
        if(strstr(list_at(lines, i), "</NORMAL>")){ read_n   = 0; continue; }
        if(strstr(list_at(lines, i), "<UV>")){ read_uv       = 1; continue;}
        if(strstr(list_at(lines, i), "</UV>")){ read_uv      = 0; continue;}

        if(strstr(list_at(lines, i), "<UVI>"))
        {
            read_uvi        = 1;
            continue;
        }
        if(strstr(list_at(lines, i), "</UVI>"))
        {
            read_uvi        = 0;
            continue;
        }
        if(read_p)
        {
            list_add(p, list_at(lines, i));
            printf("Adding p : %s\n", list_at(lines, i));
        }
        if(read_pi)
        {
            list_add(pi, list_at(lines, i));
        }
        if(read_n)
        {
            list_add(n, list_at(lines, i));
        }
        if(read_uv)
        {
            list_add(uv, list_at(lines, i));
        }
        if(read_uvi)
        {
            list_add(uvi, list_at(lines, i));
        }
    }

    for(i = 0; i < list_count(p); i++)
    {
        printf("RePrint p: %s\n", list_at(p,i));
    }

    int l_count_p       = list_count(p) / 3;
    int l_count_pi      = list_count(pi);
    int l_count_n       = list_count(n) / 3;
    int l_count_uv      = list_count(uv) / 2;
    int l_count_uvi     = list_count(uvi);

    int *pi_dat         = malloc(sizeof(int)           * l_count_pi);
    int *uvi_dat        = malloc(sizeof(int)           * l_count_uvi);
    VEC3 *p_dat         = malloc(sizeof(VEC3)      * l_count_p);

    //same mode as p_dat
    VEC4 *p_dat_weight =  malloc(sizeof(VEC4)  * l_count_p);
    Vector4DI *p_dat_indices =  malloc(sizeof(Vector4DI)  * l_count_p);

    VEC3 *n_dat     = malloc(sizeof(VEC3)  * l_count_n);
    VEC3 *uv_dat    = malloc(sizeof(VEC3)  * l_count_uv);
    VEC3  *raw_p    = malloc(sizeof(VEC3)  * l_count_pi);
    VEC3  *raw_uv   = malloc(sizeof(VEC3)  * l_count_uvi);

    VEC4  *raw_weight    = malloc(sizeof(VEC4)  * l_count_pi);
    Vector4DI *raw_weight_index   = malloc(sizeof(Vector4DI) * l_count_pi);

    for(i = 0; i < l_count_p; i++)
    {
        p_dat_weight[i].x = 0;
        p_dat_weight[i].y = 0;
        p_dat_weight[i].z = 0;
        p_dat_weight[i].w = 0;
        p_dat_indices[i].x = 0;
        p_dat_indices[i].y = 0;
        p_dat_indices[i].z = 0;
        p_dat_indices[i].w = 0;
    }

    //Fill the p_dat_weight/indices now before rolling them out
    int d,e;
    for(d = 0; d < list_count(deformers); d++)
    {
        Deformer *def = list_at(deformers, d);

        for(e = 0; e < list_count(def->weightsl); e++)
        {
            float *deformer_weight_current_p = list_at(def->weightsl,e);
            int *deformer_index_current_p = list_at(def->indicesl,e);
            float deformer_weight_current = (*deformer_weight_current_p);
            int deformer_index_current =(*deformer_index_current_p);


            if(deformer_weight_current < 0.025f)
                deformer_weight_current = 0;
            if(deformer_weight_current > 0.975f)
                deformer_weight_current = 1;

            if(p_dat_weight[deformer_index_current].x == 0)
            {
                p_dat_weight[deformer_index_current].x = deformer_weight_current;
                p_dat_indices[deformer_index_current].x = deformer_index_current;

                //printf("%f %d\n",p_dat_weight[deformer_index_current].x,p_dat_indices[deformer_index_current].x);

            }
            else if(p_dat_weight[deformer_index_current].y == 0)
            {
                p_dat_weight[deformer_index_current].y = deformer_weight_current;
                p_dat_indices[deformer_index_current].y = deformer_index_current;
                //printf("%f %d\n",p_dat_weight[deformer_index_current].y,p_dat_indices[deformer_index_current].y);
            }
            else if(p_dat_weight[deformer_index_current].z == 0)
            {
                p_dat_weight[deformer_index_current].z = deformer_weight_current;
                p_dat_indices[deformer_index_current].z = deformer_index_current;
                //printf("%f %d\n",p_dat_weight[deformer_index_current].z,p_dat_indices[deformer_index_current].z);
            }
            else
            {
                p_dat_weight[deformer_index_current].w = deformer_weight_current;
                p_dat_indices[deformer_index_current].w = deformer_index_current;
                //printf("%f %d\n",p_dat_weight[deformer_index_current].w,p_dat_indices[deformer_index_current].w);
            }
        }
        //free(list_at(deformers, d));
    }

    //Cleanup run
    for(d = 0; d < list_count(deformers); d++)
    {
        Deformer *def = list_at(deformers, d);

        for(e = 0; e < list_count(def->weightsl); e++)
        {
            free(list_at(def->weightsl,e));
            list_remove_at(def->weightsl, e);

            free(list_at(def->indicesl,e));
            list_remove_at(def->indicesl, e);
        }
        free(list_at(deformers, d));
        list_remove_at(deformers, d);
    }

    //Free all deformers here
    for(d = 0; d < l_count_p; d++)
    {
        printf("Weight %d: %.2f %.2f %.2f %.2f\n",
            d,
            p_dat_weight[d].x,
            p_dat_weight[d].y,
            p_dat_weight[d].z,
            p_dat_weight[d].w);
    }


    //Points
    for(i = 0, j=0; i < l_count_p; i++, j+=3)
    {
        p_dat[i].x      = atof(list_at(p,j+0));
        p_dat[i].y      = atof(list_at(p,j+1));
        p_dat[i].z      = atof(list_at(p,j+2));

        free(list_at(p, j+0));
        free(list_at(p, j+1));
        free(list_at(p, j+2));

        printf("Points: %f %f %f\n",  p_dat[i].x, p_dat[i].y, p_dat[i].z);
    }

    //point index
    for(i = 0; i < l_count_pi; i++)
    {
        pi_dat[i]       = atoi(list_at(pi,i));

        free(list_at(pi,i));
    }

    //normals
    for(i = 0, j=0; i < l_count_n; i++, j+=3)
    {
        int index       = i*3;
        n_dat[i].x      = atof(list_at(n,j+0));
        n_dat[i].y      = atof(list_at(n,j+1));
        n_dat[i].z      = atof(list_at(n,j+2));
        free(list_at(n, j+0));
        free(list_at(n, j+1));
        free(list_at(n, j+2));

        //printf("Normals: %f %f %f\n",  n_dat[i].x,n_dat[i].y,n_dat[i].z);
    }
    //uv
    for(i = 0, j = 0; i < l_count_uv; i++, j+=2)
    {
        uv_dat[i].x         = atof(list_at(uv,j+0));
        uv_dat[i].y         = atof(list_at(uv,j+1));
        free(list_at(uv,    j+0));
        free(list_at(uv,    j+1));

        //printf("UV: %f %f\n",  uv_dat[i].x,uv_dat[i].y);
    }

    //uv index
    for(i = 0; i < l_count_uvi; i++)
    {
        uvi_dat[i]      = atoi(list_at(uvi,i));
        free(list_at(uvi,   i));
    }

    //Convert from indexed mode to full lists
    for(i = 0; i < l_count_pi; i++)
    {
        raw_p[i].x      = p_dat[pi_dat[i]].x;
        raw_p[i].y      = p_dat[pi_dat[i]].y;
        raw_p[i].z      = p_dat[pi_dat[i]].z;

        raw_weight[i].x = p_dat_weight[pi_dat[i]].x;
        raw_weight[i].y = p_dat_weight[pi_dat[i]].y;
        raw_weight[i].z = p_dat_weight[pi_dat[i]].z;
        raw_weight[i].w = p_dat_weight[pi_dat[i]].w;
        raw_weight_index[i].x = p_dat_indices[pi_dat[i]].x;
        raw_weight_index[i].y = p_dat_indices[pi_dat[i]].y;
        raw_weight_index[i].z = p_dat_indices[pi_dat[i]].z;
        raw_weight_index[i].w = p_dat_indices[pi_dat[i]].w;

        /*
        printf("P_raw: %.2f %.2f %.2f. W: %.2f, %.2f, %.2f, %.2f\n",
                raw_p[i].x, raw_p[i].y, raw_p[i].z,
                raw_weight[i].x,
                raw_weight[i].y,
                raw_weight[i].z,
                raw_weight[i].w);
        */
    }
    for(i = 0; i < l_count_uvi; i++)
    {
        raw_uv[i].x     = uv_dat[uvi_dat[i]].x;
        raw_uv[i].y     = uv_dat[uvi_dat[i]].y;

        //printf("UV_raw: %f %f\n", raw_uv[i].x,raw_uv[i].y);
    }

    m->w_size           = sizeof(VEC4)  * l_count_pi;
    m->wi_size          = sizeof(Vector4DI)  * l_count_pi;
    m->p_size           = sizeof(VEC3) * l_count_pi;
    m->uv_size          = sizeof(VEC2) * l_count_uvi;
    m->n_size           = sizeof(VEC3) * l_count_n;

    m->p_id             = create_buffer_float(m->p_size, (float*)raw_p);
    m->uv_id            = create_buffer_float(m->uv_size,(float*)raw_uv);
    m->n_id             = create_buffer_float(m->n_size, (float*)n_dat);

    m->w_id             = create_buffer_float(m->n_size, (float*)raw_weight);
    m->wi_id            = create_buffer_float(m->n_size, (float*)raw_weight_index);
    free(raw_p);
    free(raw_uv);
    free(n_dat);
    free(raw_weight_index);
    free(raw_weight);
    m->vertex_count = l_count_pi;
    return m;
}

Connections *load_connections(char *filename)
{
    Connections     *c;
    _LIST           *lines;
    char            *buf;
    int              i;

    c               = malloc(sizeof(Connections));
    c->names        = list_new();
    c->parents      = list_new();

    buf             = load_text(filename);
    lines           = split_buffer(buf, "\n");

    for( i = 0; i < list_count(lines); i++)
    {
        int len         = strlen(list_at(lines, i))+sizeof(char);
        char *line      = malloc(len);
        strcpy(line,    list_at(lines, i));

        line[len-1] = '\0';
        char *parent    = strtok(line," ");
        char *name      =  strtok(NULL, " ");

        printf("%s %s\n",       parent, name);
        list_add(c->parents,    parent);
        list_add(c->names,      name);
    }
    return c;
}

Animation *load_animation(char *filename, char *anim_name, int anim_index)
{
    char *buf;
    int d, e;
    int i;
    Animation* animation;

    _LIST *joints,*lines;
    Joint_Key *k;
    Joint_Key_List *temp_joint_key;
    _LIST *new_joints;

    animation       = malloc(sizeof(Animation));
    joints          = list_new();
    new_joints      = list_new();

    buf             = load_text(filename);
    lines           = split_buffer(buf, "\n");

    //Switches
    int read_trans_x_val;
    int read_trans_x_dis;
    int read_trans_y_val;
    int read_trans_y_dis;
    int read_trans_z_val;
    int read_trans_z_dis;

    int read_rot_x_val;
    int read_rot_x_dis;
    int read_rot_y_val;
    int read_rot_y_dis;
    int read_rot_z_val;
    int read_rot_z_dis;

    int read_scale_x_val;
    int read_scale_x_dis;
    int read_scale_y_val;
    int read_scale_y_dis;
    int read_scale_z_val;
    int read_scale_z_dis;

    //Switches
    read_trans_x_val = 0;
    read_trans_x_dis = 0;
    read_trans_y_val = 0;
    read_trans_y_dis = 0;
    read_trans_z_val = 0;
    read_trans_z_dis = 0;

    read_rot_x_val = 0;
    read_rot_x_dis = 0;
    read_rot_y_val = 0;
    read_rot_y_dis = 0;
    read_rot_z_val = 0;
    read_rot_z_dis = 0;

    read_scale_x_val = 0;
    read_scale_x_dis = 0;
    read_scale_y_val = 0;
    read_scale_y_dis = 0;
    read_scale_z_val = 0;
    read_scale_z_dis = 0;

    for(i = 0; i < list_count(lines); i++)
    {
        if(strstr(list_at(lines,i), "<JOINT>"))
        {
            temp_joint_key = malloc(sizeof(Joint_Key));
            temp_joint_key->tx = list_new();
            temp_joint_key->ty = list_new();
            temp_joint_key->tz = list_new();
            temp_joint_key->rx = list_new();
            temp_joint_key->ry = list_new();
            temp_joint_key->rz = list_new();
            temp_joint_key->sx = list_new();
            temp_joint_key->sy = list_new();
            temp_joint_key->sz = list_new();
            temp_joint_key->txd = list_new();
            temp_joint_key->tyd = list_new();
            temp_joint_key->tzd = list_new();
            temp_joint_key->rxd = list_new();
            temp_joint_key->ryd = list_new();
            temp_joint_key->rzd = list_new();
            temp_joint_key->sxd = list_new();
            temp_joint_key->syd = list_new();
            temp_joint_key->szd = list_new();

             continue;
        }
        if(strstr(list_at(lines,i), "NAME:"))
        {
            int len         = strlen(list_at(lines, i))+sizeof(char);
            char *line      = malloc(len);
            strcpy(line,    list_at(lines, i));

            line[len-1] = '\0';
            char *trash    = strtok(line," ");
            char *name      =  strtok(NULL, " ");

            printf("Name is: %s\n", name);
            temp_joint_key->name = name;
            continue;
        }

        //All TRANSLATION data is read here
        {
            //TRANS X
            if(strstr(list_at(lines,i), "<TRANS_X_VAL>"))
            {
                read_trans_x_val = 1;
                continue;
            }
            if(strstr(list_at(lines,i), "</TRANS_X_VAL>"))
            {
                read_trans_x_val = 0;
                continue;
            }
            if(strstr(list_at(lines,i), "<TRANS_X_DIS>"))
            {
                read_trans_x_dis = 1;
                continue;
            }
            if(strstr(list_at(lines,i), "</TRANS_X_DIS>"))
            {
                read_trans_x_dis = 0;
                continue;
            }

            //TRANS Y
            if(strstr(list_at(lines,i), "<TRANS_Y_VAL>"))
            {
                read_trans_y_val = 1;
                continue;
            }
            if(strstr(list_at(lines,i), "</TRANS_Y_VAL>"))
            {
                read_trans_y_val = 0;
                continue;
            }
            if(strstr(list_at(lines,i), "<TRANS_Y_DIS>"))
            {
                read_trans_y_dis = 1;
                continue;
            }
            if(strstr(list_at(lines,i), "</TRANS_Y_DIS>"))
            {
                read_trans_y_dis = 0;
                continue;
            }

            //TRANS Z
            if(strstr(list_at(lines,i), "<TRANS_Z_VAL>"))
            {
                read_trans_z_val = 1;
                continue;
            }
            if(strstr(list_at(lines,i), "</TRANS_Z_VAL>"))
            {
                read_trans_z_val = 0;
                continue;
            }
            if(strstr(list_at(lines,i), "<TRANS_Z_DIS>"))
            {
                read_trans_z_dis = 1;
                continue;
            }
            if(strstr(list_at(lines,i), "</TRANS_Z_DIS>"))
            {
                read_trans_z_dis = 0;
                continue;
            }
        }

        //All ROTATION data is read here
        {
            //ROT X
            if(strstr(list_at(lines,i), "<ROT_X_VAL>"))
            {
                read_rot_x_val = 1;
                continue;
            }
            if(strstr(list_at(lines,i), "</ROT_X_VAL>"))
            {
                read_rot_x_val = 0;
                continue;
            }
            if(strstr(list_at(lines,i), "<ROT_X_DIS>"))
            {
                read_rot_x_dis = 1;
                continue;
            }
            if(strstr(list_at(lines,i), "</ROT_X_DIS>"))
            {
                read_rot_x_dis = 0;
                continue;
            }

            //ROT Y
            if(strstr(list_at(lines,i), "<ROT_Y_VAL>"))
            {
                read_rot_y_val = 1;
                continue;
            }
            if(strstr(list_at(lines,i), "</ROT_Y_VAL>"))
            {
                read_rot_y_val = 0;
                continue;
            }
            if(strstr(list_at(lines,i), "<ROT_Y_DIS>"))
            {
                read_rot_y_dis= 1;
                continue;
            }
            if(strstr(list_at(lines,i), "</ROT_Y_DIS>"))
            {
                read_rot_y_dis = 0;
                continue;
            }


            //ROT Z
            if(strstr(list_at(lines,i), "<ROT_Z_VAL>"))
            {
                read_rot_z_val = 1;
                continue;
            }
            if(strstr(list_at(lines,i), "</ROT_Z_VAL>"))
            {
                read_rot_z_val = 0;
                continue;
            }

            if(strstr(list_at(lines,i), "<ROT_Z_DIS>"))
            {
                read_rot_z_dis = 1;
                continue;
            }
            if(strstr(list_at(lines,i), "</ROT_Z_DIS>"))
            {
                read_rot_z_dis = 0;
                continue;
            }
        }

        //All SCALE data is read here
        {
            //SCALE X
            if(strstr(list_at(lines,i), "<SCALE_X_VAL>"))
            {
                read_scale_x_val = 1;
                continue;
            }
            if(strstr(list_at(lines,i), "</SCALE_X_VAL>"))
            {
                read_scale_x_val = 0;
                continue;
            }
            if(strstr(list_at(lines,i), "<SCALE_X_DIS>"))
            {
                read_scale_x_dis = 1;
                continue;
            }
            if(strstr(list_at(lines,i), "</SCALE_X_DIS>"))
            {
                read_scale_x_dis = 0;
                continue;
            }

            //SCALE Y
            if(strstr(list_at(lines,i), "<SCALE_Y_VAL>"))
            {
                read_scale_y_val = 1;
                continue;
            }
            if(strstr(list_at(lines,i), "</SCALE_Y_VAL>"))
            {
                read_scale_y_val = 0;
                continue;
            }
            if(strstr(list_at(lines,i), "<SCALE_Y_DIS>"))
            {
                read_scale_y_dis= 1;
                continue;
            }
            if(strstr(list_at(lines,i), "</SCALE_Y_DIS>"))
            {
                read_scale_y_dis = 0;
                continue;
            }

            //SCALE Z
            if(strstr(list_at(lines,i), "<SCALE_Z_VAL>"))
            {
                read_scale_z_val = 1;
                continue;
            }
            if(strstr(list_at(lines,i), "</SCALE_Z_VAL>"))
            {
                read_scale_z_val = 0;
                continue;
            }

            if(strstr(list_at(lines,i), "<SCALE_Z_DIS>"))
            {
                read_scale_z_dis = 1;
                continue;
            }
            if(strstr(list_at(lines,i), "</SCALE_Z_DIS>"))
            {
                read_scale_z_dis = 0;
                continue;
            }
        }

        //Read the data into the joint_key_values
        {
            //For trans X
            if(read_trans_x_val)
            {
                char *v = list_at(lines,i);
                char *theline = v;
                float *val  = malloc(sizeof(float));
                (*val) = atof(v);
                list_add(temp_joint_key->tx, val);
                printf("%f\n", *val);
                continue;
            }
            if(read_trans_x_dis)
            {
                char *v = list_at(lines,i);
                int *val  = malloc(sizeof(int));
                (*val) = atoi(v);
                list_add(temp_joint_key->txd, val);
                printf("%d\n", *val);
            }

            //For trans Y
            if(read_trans_y_val)
            {
                char *v = list_at(lines,i);
                float *val  = malloc(sizeof(float));
                (*val) = atof(v);
                list_add(temp_joint_key->ty, val);
                printf("%f\n", *val);
            }
            if(read_trans_y_dis)
            {
                char *v = list_at(lines,i);
                int *val  = malloc(sizeof(int));
                (*val) = atoi(v);
                list_add(temp_joint_key->tyd, val);
                printf("%d\n", *val);
            }

            //For trans Z
            if(read_trans_z_val)
            {
                char *v = list_at(lines,i);
                float *val  = malloc(sizeof(float));
                (*val) = atof(v);
                list_add(temp_joint_key->tz, val);
                printf("%f\n", *val);
            }
            if(read_trans_z_dis)
            {
                char *v = list_at(lines,i);
                int *val  = malloc(sizeof(int));
                (*val) = atoi(v);
                list_add(temp_joint_key->tzd, val);
                printf("%d\n", *val);
            }

            //For rotation X
            if(read_rot_x_val)
            {
                char *v = list_at(lines,i);
                float *val  = malloc(sizeof(float));
                (*val) = atof(v);
                list_add(temp_joint_key->rx, val);
                printf("%f\n", *val);
            }
            if(read_rot_x_dis)
            {
                char *v = list_at(lines,i);
                int *val  = malloc(sizeof(int));
                (*val) = atoi(v);
                list_add(temp_joint_key->rxd, val);
                printf("%d\n", *val);
            }
            //For rotation Y
            if(read_rot_y_val)
            {
                char *v = list_at(lines,i);
                float *val  = malloc(sizeof(float));
                (*val) = atof(v);
                list_add(temp_joint_key->ry, val);
                printf("%f\n", *val);
            }
            if(read_rot_y_dis)
            {
                char *v = list_at(lines,i);
                int *val  = malloc(sizeof(int));
                (*val) = atoi(v);
                list_add(temp_joint_key->ryd, val);
                printf("%d\n", *val);
            }

            //For rotation z
            if(read_rot_z_val)
            {
                char *v = list_at(lines,i);
                float *val  = malloc(sizeof(float));
                (*val) = atof(v);
                list_add(temp_joint_key->rz, val);
                printf("%f\n", *val);
            }
            if(read_rot_z_dis)
            {
                char *v = list_at(lines,i);
                int *val  = malloc(sizeof(int));
                (*val) = atoi(v);
                list_add(temp_joint_key->rzd, val);
                printf("%d\n", *val);
            }

            //For scale X
            if(read_scale_x_val)
            {
                char *v = list_at(lines,i);
                float *val  = malloc(sizeof(float));
                (*val) = atof(v);
                list_add(temp_joint_key->sx, val);
                printf("%f\n", *val);
            }
            if(read_scale_x_dis)
            {
                char *v = list_at(lines,i);
                int *val  = malloc(sizeof(int));
                (*val) = atoi(v);
                list_add(temp_joint_key->sxd, val);
                printf("%d\n", *val);
            }
            //For scale Y
            if(read_scale_y_val)
            {
                char *v = list_at(lines,i);
                float *val  = malloc(sizeof(float));
                (*val) = atof(v);
                list_add(temp_joint_key->sy, val);
                printf("%f\n", *val);
            }
            if(read_scale_y_dis)
            {
                char *v = list_at(lines,i);
                int *val  = malloc(sizeof(int));
                (*val) = atoi(v);
                list_add(temp_joint_key->syd, val);
                printf("%d\n", *val);
            }
            //For scale z
            if(read_scale_z_val)
            {
                char *v = list_at(lines,i);
                float *val  = malloc(sizeof(float));
                (*val) = atof(v);
                list_add(temp_joint_key->sz, val);
                printf("%f\n", *val);

            }
            if(read_scale_z_dis)
            {
                char *v = list_at(lines,i);
                int *val  = malloc(sizeof(int));
                (*val) = atoi(v);
                list_add(temp_joint_key->szd, val);
                printf("%d\n", *val);
            }
        }
        if(strstr(list_at(lines,i), "</JOINT>"))
        {
            list_add(joints, temp_joint_key);
            temp_joint_key = NULL;
            continue;
        }
    }

    int joint_count = list_count(joints);
    new_joints = list_new();
    Joint_Key *key_nolist;

    for(d = 0; d < list_count(joints); d++)
    {
        Joint_Key_List *k = list_at(joints,d);
        key_nolist = malloc(sizeof(Joint_Key));

        key_nolist->name = k->name;

        key_nolist->tx = malloc(sizeof(float)*list_count(k->tx));
        key_nolist->ty = malloc(sizeof(float)*list_count(k->ty));
        key_nolist->tz = malloc(sizeof(float)*list_count(k->tz));
        key_nolist->txd = malloc(sizeof(int)*list_count(k->txd));
        key_nolist->tyd = malloc(sizeof(int)*list_count(k->txd));
        key_nolist->tzd = malloc(sizeof(int)*list_count(k->txd));
        key_nolist->t_count = list_count(k->tx);
        key_nolist->td_count = list_count(k->txd);

        key_nolist->rx = malloc(sizeof(float)*list_count(k->rx));
        key_nolist->ry = malloc(sizeof(float)*list_count(k->ry));
        key_nolist->rz = malloc(sizeof(float)*list_count(k->rz));
        key_nolist->rxd = malloc(sizeof(int)*list_count(k->rxd));
        key_nolist->ryd = malloc(sizeof(int)*list_count(k->ryd));
        key_nolist->rzd = malloc(sizeof(int)*list_count(k->rzd));
        key_nolist->r_count = list_count(k->rx);
        key_nolist->rd_count = list_count(k->rxd);

        key_nolist->sx = malloc(sizeof(float)*list_count(k->sx));
        key_nolist->sy = malloc(sizeof(float)*list_count(k->sy));
        key_nolist->sz = malloc(sizeof(float)*list_count(k->sz));
        key_nolist->sxd = malloc(sizeof(int)*list_count(k->sxd));
        key_nolist->syd = malloc(sizeof(int)*list_count(k->syd));
        key_nolist->szd = malloc(sizeof(int)*list_count(k->szd));

        key_nolist->s_count = list_count(k->sx);
        key_nolist->sd_count = list_count(k->sxd);

        for(e = 0; e < list_count(k->tx); e++)
        {

            float *v = list_at(k->tx, e);
            float vv = (*v);
            key_nolist->tx[e] = vv;

            free( list_at(k->tx,e));
            printf("Joint: %d: Trans X Val: %f\n", d,vv);
        }
        for(e = 0; e < list_count(k->txd); e++)
        {
            int *v = list_at(k->txd, e);
            int vv = (*v);
            key_nolist->txd[e] = vv;
            free(list_at(k->txd,e));
            printf("Joint: %d: Trans X Dis: %d\n", d,vv);
        }
        for(e = 0; e < list_count(k->ty); e++)
        {
            float *v = list_at(k->ty, e);
            float vv = (*v);
            key_nolist->ty[e] = vv;
            free(list_at(k->ty,e));
            printf("Joint: %d: Trans Y Val: %f\n", d,vv);
        }
        for(e = 0; e < list_count(k->tyd); e++)
        {
            int *v = list_at(k->tyd, e);
            int vv = (*v);
            key_nolist->tyd[e] = vv;
            free(list_at(k->tyd,e));
            printf("Joint: %d: Trans Y Dis: %d\n", d,vv);
        }
        for(e = 0; e < list_count(k->tz); e++)
        {
            float *v = list_at(k->tz, e);
            float vv = (*v);
            key_nolist->tz[e] = vv;
            free(list_at(k->tz,e));
            printf("Joint: %d: Trans Z Val: %f\n", d,vv);
        }
        for(e = 0; e < list_count(k->tzd); e++)
        {
            int *v = list_at(k->tzd, e);
            int vv = (*v);
            key_nolist->tzd[e] = vv;
            free(list_at(k->tzd, e));
            printf("Joint: %d: Trans Z Dis: %d\n", d,vv);
        }

        //Rot x
        for(e = 0; e < list_count(k->rx); e++)
        {
            float *v = list_at(k->rx, e);
            float vv = (*v);
            key_nolist->rx[e] = vv;
            free(list_at(k->rx,e));
            printf("Joint: %d: Rotation X Val: %f\n", d,vv);
        }
        for(e = 0; e < list_count(k->rxd); e++)
        {
            int *v = list_at(k->rxd, e);
            int vv = (*v);
            key_nolist->rxd[e] = vv;
            free(list_at(k->rxd,e));
            printf("Joint: %d: Rotation X Dis: %d\n", d,vv);
        }
        for(e = 0; e < list_count(k->ry); e++)
        {
            float *v = list_at(k->ry, e);
            float vv = (*v);
            key_nolist->ry[e] = vv;
            free(list_at(k->ry,e));
            printf("Joint: %d: Rotation Y Val: %f\n", d,vv);
        }
        for(e = 0; e < list_count(k->ryd); e++)
        {
            int *v = list_at(k->ryd, e);
            int vv = (*v);
            key_nolist->ryd[e] = vv;
            free(list_at(k->ryd,e));
            printf("Joint: %d: Rotation Y Dis: %d\n", d,vv);
        }
        for(e = 0; e < list_count(k->rz); e++)
        {
            float *v = list_at(k->rz, e);
            float vv = (*v);
            key_nolist->rz[e] = vv;
            free(list_at(k->rz,e));
            printf("Joint: %d: Rotation Z Val: %f\n", d,vv);
        }
        for(e = 0; e < list_count(k->rzd); e++)
        {
            int *v = list_at(k->rzd, e);
            int vv = (*v);
            key_nolist->rzd[e] = vv;
            free(list_at(k->rzd,e));
            printf("Joint: %d: Rotation Z Dis: %d\n", d,vv);
        }

        //Scale x
        for(e = 0; e < list_count(k->sx); e++)
        {
            float *v = list_at(k->sx, e);
            float vv = (*v);
            key_nolist->sx[e] = vv;
            free(list_at(k->sx,e));
            printf("Joint: %d: Scale X Val: %f\n", d,vv);
        }
        for(e = 0; e < list_count(k->sxd); e++)
        {
            int *v = list_at(k->sxd, e);
            int vv = (*v);
            key_nolist->sxd[e] = vv;
            free(list_at(k->sxd,e));
            printf("Joint: %d: Scale X Dis: %d\n", d,vv);
        }
        for(e = 0; e < list_count(k->sy); e++)
        {
            float *v = list_at(k->sy, e);
            float vv = (*v);
            key_nolist->sy[e] = vv;
            free(list_at(k->sy,e));
            printf("Joint: %d: Scale Y Val: %f\n", d,vv);
        }
        for(e = 0; e < list_count(k->syd); e++)
        {
            int *v = list_at(k->syd, e);
            int vv = (*v);
            key_nolist->syd[e] = vv;
            free(list_at(k->syd,e));
            printf("Joint: %d: Scale Y Dis: %d\n", d,vv);
        }
        for(e = 0; e < list_count(k->sz); e++)
        {
            float *v = list_at(k->sz, e);
            float vv = (*v);
            key_nolist->sz[e] = vv;
            free(list_at(k->sz,e));
            printf("Joint: %d: Scale Z Val: %f\n", d,vv);
        }
        for(e = 0; e < list_count(k->szd); e++)
        {
            int *v = list_at(k->szd, e);
            int vv = (*v);
            key_nolist->szd[e] = vv;
            free(list_at(k->szd,e));
            printf("Joint: %d: Scale Z Dis: %d\n", d,vv);
        }
        list_add(new_joints, key_nolist);

        list_clear(k->tx);
        list_clear(k->ty);
        list_clear(k->tz);
        list_clear(k->txd);
        list_clear(k->txd);
        list_clear(k->txd);

        list_clear(k->rx);
        list_clear(k->ry);
        list_clear(k->rz);
        list_clear(k->rxd);
        list_clear(k->ryd);
        list_clear(k->rzd);

        list_clear(k->sx);
        list_clear(k->sy);
        list_clear(k->sz);
        list_clear(k->sxd);
        list_clear(k->syd);
        list_clear(k->szd);
        k->name = NULL;
        free(k);
    }


    for(d = 0; d < joint_count; d++)
    {
        Joint_Key *entry = list_at(new_joints, d);

        for(e = 0; e < entry->t_count; e++)
        {
            printf("printing t_x: %f\n", entry->tx[e]);
        }
        for(e = 0; e < entry->t_count; e++)
        {
            printf("printing t_y: %f\n", entry->ty[e]);
        }
        for(e = 0; e < entry->t_count; e++)
        {
            printf("printing t_z: %f\n", entry->tz[e]);
        }
        for(e = 0; e < entry->r_count; e++)
        {
            printf("printing r_x: %f\n", entry->rx[e]);
        }
        for(e = 0; e < entry->r_count; e++)
        {
            printf("printing r_y: %f\n", entry->ry[e]);
        }
        for(e = 0; e < entry->r_count; e++)
        {
            printf("printing r_z: %f\n", entry->rz[e]);
        }

        for(e = 0; e < entry->s_count; e++)
        {
            printf("printing s_x: %f\n", entry->sx[e]);
        }
        for(e = 0; e < entry->s_count; e++)
        {
            printf("printing s_y: %f\n", entry->sy[e]);
        }
        for(e = 0; e < entry->s_count; e++)
        {
            printf("printing s_z: %f\n", entry->sz[e]);
        }
    }

    animation->joint = new_joints;
    animation->animation_name = anim_name;
    animation->animation_index = anim_index;


    return animation;
}

//This function sums the functions above
