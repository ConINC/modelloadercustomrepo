#include <iostream>
#include "ModelLoader.h"

using namespace std;

int main()
{
    cout << "Hello world!" << endl;

    _Model_Static stat = ModelLoader::load_model_static("model_static");

    _Model_Skinned skin = ModelLoader::load_model_skinned("model_static");
    std::cout << "Loaded model static: " << stat.name << std::endl;
    std::cout << "Loaded model skinned: " << skin.name<< std::endl;


    return 0;
}
