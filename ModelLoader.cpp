#include "ModelLoader.h"

ModelLoader::ModelLoader()
{
    //ctor
}

ModelLoader::~ModelLoader()
{
    //dtor
}

std::vector<std::string> ModelLoader::Get_Lines(std::string filename)
{
    std::ifstream in(filename);
    std::vector<std::string> lines;
    std::string line;

    if(!in)
    {
        std::cout<< "Failed to open Filestream to: " << filename << std::endl;
        return lines;
    }

    while (std::getline(in, line))
    {
        lines.push_back(line);
    }
    //std::cout<< "Succesfully opened: " << filename << std::endl;

    return lines;
}


_Connections ModelLoader::load_connections(std::string filename)
{
    _Connections  con;
    std::vector<std::string> lines;
    lines = Get_Lines(filename);

    std::vector<std::string> parents;
    std::vector<std::string> childs;
    std::istringstream f;

    for(int i = 0; i < lines.size(); i++)
    {
        std::string parent;
        std::string child;
        std::string line = lines.at(i);

        f.str(line);
        f.clear();
        getline(f,parent, ' ');
        getline(f,child , ' ');
        parents.push_back(parent);
        parents.push_back(child);
        std::cout << parent << " : " << child << std::endl;
    }

    return con;
}


std::vector<class _Deformer> ModelLoader::load_deformers(std::string filename)
{
    std::vector<std::string> lines;
    std::vector<class _Deformer> deformers;
    _Deformer temp_deformer;
    std::istringstream f;
    lines = Get_Lines(filename);
    if(lines.size() < 1)
        return deformers;

    bool read_weight = false;
    bool read_index = false;

    for(int i = 0; i < lines.size(); i++)
    {
        std::string line = lines.at(i);

        ///Start and End tags of Deformer node
        if(line.find("<DEFORMER>") != std::string::npos)
        {
            temp_deformer.name = "";
            continue;
        }

        if(line.find("</DEFORMER>") != std::string::npos)
        {
            deformers.push_back(temp_deformer);
            temp_deformer.indices.clear();
            temp_deformer.weights.clear();
            continue;
        }

        ///Copy name to the current deformer
        if(line.find("NAME: ") != std::string::npos)
        {
            std::string s;
            f.str(line);
            f.clear();

            getline(f, s, ' ');
            getline(f,temp_deformer.name , ' ');
            continue;
        }

        ///Weight and Index read switches here
        if(line.find("<WEIGHT>") != std::string::npos)
        {
            read_weight = true;
            continue;
        }
        if(line.find("</WEIGHT>") != std::string::npos)
        {
            read_weight = false;
            continue;
        }
        if(line.find("<INDEX>") != std::string::npos)
        {
            read_index = true;
            continue;
        }
        if(line.find("</INDEX>") != std::string::npos)
        {
            read_index = false;
            continue;
        }

        if(read_weight)
        {
            temp_deformer.weights.push_back(atof(line.c_str()));
            continue;
        }
        if(read_index)
        {
            temp_deformer.indices.push_back(atoi(line.c_str()));
            continue;
        }
    }

    ///Check Deformers list
    std::cout << "\n-------------------------------------" << std::endl;
    std::cout << "Printing Deformers                   " << std::endl;
    std::cout << "-------------------------------------" << std::endl;
    for(int i = 0; i < deformers.size(); i++)
    {
        std::cout << "Name: " <<  deformers.at(i).name << std::endl;

        for(int j = 0; j < deformers.at(i).weights.size(); j++)
        {
            std::cout << "Weight: " <<  deformers.at(i).weights.at(j) << std::endl;
        }

        for(int j = 0; j < deformers.at(i).weights.size(); j++)
        {
            std::cout << "Index: " <<  deformers.at(i).indices.at(j)<< std::endl;
        }
    }
    std::cout << "-------------------------------------" << std::endl;

    return deformers;
}

void ModelLoader::print_vector_float(std::vector<float> vec)
{
    for(int i = 0; i < vec.size(); i++)
    {
        std::cout << vec.at(i) << " ";
    }
    std::cout << std::endl;
}

void ModelLoader::print_vector_vec3(std::vector<_Vector3D> vec)
{
    for(int i = 0; i < vec.size(); i++)
    {
        std::cout << "Vec3: " << vec.at(i).x << " " << vec.at(i).y << " "  << vec.at(i).z << std::endl;
    }
    std::cout << std::endl;
}

void ModelLoader::print_vector_vec2(std::vector<_Vector2D> vec)
{
    for(int i = 0; i < vec.size(); i++)
    {
        std::cout << "Vec2: " << vec.at(i).x << " " << vec.at(i).y << std::endl;
    }
    std::cout << std::endl;
}

void ModelLoader::print_vector_int(std::vector<int> vec)
{
    for(int i = 0; i < vec.size(); i++)
    {
        std::cout << vec.at(i) << " ";
    }
    std::cout << std::endl;
}

_Animation ModelLoader::load_animation(std::string filename, std::string name, int index)
{
    _Animation animation;
    _Joint_fullframes temp;

    std::vector<std::string> lines = Get_Lines(filename);
    if(lines.size() < 1)
        return animation;

    std::istringstream f;
    std::string line;
    animation.animation_name = name;
    animation.animation_index = index;
    //Switches
    bool read_trans_x_val;
    bool read_trans_x_dis;
    bool read_trans_y_val;
    bool read_trans_y_dis;
    bool read_trans_z_val;
    bool read_trans_z_dis;

    bool read_rot_x_val;
    bool read_rot_x_dis;
    bool read_rot_y_val;
    bool read_rot_y_dis;
    bool read_rot_z_val;
    bool read_rot_z_dis;

    bool read_scale_x_val;
    bool read_scale_x_dis;
    bool read_scale_y_val;
    bool read_scale_y_dis;
    bool read_scale_z_val;
    bool read_scale_z_dis;


    for(int i = 0; i < lines.size(); i++)
    {
        std::cout << line << std::endl;

        line =  lines.at(i);
        if(line.find("<JOINT>") != std::string::npos)
        {
            temp.tx.clear();    temp.ty.clear();    temp.tz.clear();
            temp.rx.clear();    temp.ry.clear();    temp.rz.clear();
            temp.sx.clear();    temp.sy.clear();    temp.sz.clear();

            temp.txd.clear();   temp.tyd.clear();   temp.tzd.clear();
            temp.rxd.clear();   temp.ryd.clear();   temp.rzd.clear();
            temp.sxd.clear();   temp.syd.clear();   temp.szd.clear();
            temp.name = "";

            continue;
        }
        if(line.find("</JOINT>") != std::string::npos)
        {
            animation.joints.push_back(temp);

            continue;
        }
        if(line.find("NAME: ") != std::string::npos)
        {
            std::string s;
            f.str(line);
            f.clear();

            getline(f, s, ' ');
            getline(f,temp.name , ' ');
            continue;
        }


        //Translation
        //TRANS X
        if(line.find("<TRANS_X_VAL>") != std::string::npos)
        {
            read_trans_x_val = true;
            continue;
        }
        if(line.find("</TRANS_X_VAL>") != std::string::npos)
        {
            read_trans_x_val = false;
            continue;
        }
        if(line.find("<TRANS_X_DIS>") != std::string::npos)
        {
            read_trans_x_dis = true;
            continue;
        }
        if(line.find("</TRANS_X_DIS>") != std::string::npos)
        {
            read_trans_x_dis = false;
            continue;
        }



        //Trans Y
        if(line.find("<TRANS_Y_VAL>") != std::string::npos)
        {
            read_trans_y_val = true;
            continue;
        }
        if(line.find("</TRANS_Y_VAL>") != std::string::npos)
        {
            read_trans_y_val = false;
            continue;
        }
        if(line.find("<TRANS_Y_DIS>") != std::string::npos)
        {
            read_trans_y_dis = true;
            continue;
        }
        if(line.find("</TRANS_Y_DIS>") != std::string::npos)
        {
            read_trans_y_dis = false;
            continue;
        }


        //TRANS Z
        if(line.find("<TRANS_Z_VAL>") != std::string::npos)
        {
            read_trans_z_val = true;
            continue;
        }
        if(line.find("</TRANS_Z_VAL>") != std::string::npos)
        {
            read_trans_z_val = false;
            continue;
        }
        if(line.find("<TRANS_Z_DIS>") != std::string::npos)
        {
            read_trans_z_dis = true;
            continue;
        }
        if(line.find("</TRANS_Z_DIS>") != std::string::npos)
        {
            read_trans_z_dis = false;
            continue;
        }



        //Rotation
        //Rotation X
        if(line.find("<ROT_X_VAL>") != std::string::npos)
        {
            read_rot_x_val = true;
            continue;
        }
        if(line.find("</ROT_X_VAL>") != std::string::npos)
        {
            read_rot_x_val = false;
            continue;
        }
        if(line.find("<ROT_X_DIS>") != std::string::npos)
        {
            read_rot_x_dis = true;
            continue;
        }
        if(line.find("</ROT_X_DIS>") != std::string::npos)
        {
            read_rot_x_dis = false;
            continue;
        }

        //Rotation Y
        if(line.find("<ROT_Y_VAL>") != std::string::npos)
        {
            read_rot_y_val = true;
            continue;
        }
        if(line.find("</ROT_Y_VAL>") != std::string::npos)
        {
            read_rot_y_val = false;
            continue;
        }
        if(line.find("<ROT_Y_DIS>") != std::string::npos)
        {
            read_rot_y_dis = true;
            continue;
        }
        if(line.find("</ROT_Y_DIS>") != std::string::npos)
        {
            read_rot_y_dis = false;
            continue;
        }



        //Rotation Z
        if(line.find("<ROT_Z_VAL>") != std::string::npos)
        {
            read_rot_z_val = true;
            continue;
        }
        if(line.find("</ROT_Z_VAL>") != std::string::npos)
        {
            read_rot_z_val = false;
            continue;
        }
        if(line.find("<ROT_Z_DIS>") != std::string::npos)
        {
            read_rot_z_dis = true;
            continue;
        }
        if(line.find("</ROT_Z_DIS>") != std::string::npos)
        {
            read_rot_z_dis = false;
            continue;
        }




        //Scale
        //Scale X
        if(line.find("<SCALE_X_VAL>") != std::string::npos)
        {
            read_scale_x_val = true;
            continue;
        }
        if(line.find("</SCALE_X_VAL>") != std::string::npos)
        {
            read_scale_x_val = false;
            continue;
        }
        if(line.find("<SCALE_X_DIS>") != std::string::npos)
        {
            read_scale_x_dis = true;
            continue;
        }
        if(line.find("</SCALE_X_DIS>") != std::string::npos)
        {
            read_scale_x_dis = false;
            continue;
        }



        //Scale Y
        if(line.find("<SCALE_Y_VAL>") != std::string::npos)
        {
            read_scale_y_val = true;
            continue;
        }
        if(line.find("</SCALE_Y_VAL>") != std::string::npos)
        {
            read_scale_y_val = false;
            continue;
        }
        if(line.find("<SCALE_Y_DIS>") != std::string::npos)
        {
            read_scale_y_dis = true;
            continue;
        }
        if(line.find("</SCALE_Y_DIS>") != std::string::npos)
        {
            read_scale_y_dis = false;
            continue;
        }

        //Scale Z
        if(line.find("<SCALE_Z_VAL>") != std::string::npos)
        {
            read_scale_z_val = true;
            continue;
        }
        if(line.find("</SCALE_Z_VAL>") != std::string::npos)
        {
            read_scale_z_val = false;
            continue;
        }
        if(line.find("<SCALE_Z_DIS>") != std::string::npos)
        {
            read_scale_z_dis = true;
            continue;
        }
        if(line.find("</SCALE_Z_DIS>") != std::string::npos)
        {
            read_scale_z_dis = false;
            continue;
        }

        //Translation comes now
        if(read_trans_x_val)
        {
            std::string s;
            f.str(line);
            f.clear();
            getline(f, s, ' ');
            getline(f, s,' ');
            temp.tx.push_back(atof( s.c_str()));
            continue;
        }

        if(read_trans_x_dis)
        {
            std::string s;
            f.str(line);
            f.clear();
            getline(f, s, ' ');
            getline(f, s,' ');
            temp.txd.push_back(atof( s.c_str()));
            continue;
        }


        if(read_trans_y_val)
        {
            std::string s;
            f.str(line);
            f.clear();
            getline(f, s, ' ');
            getline(f, s,' ');
            temp.ty.push_back(atof( s.c_str()));
            continue;
        }

        if(read_trans_y_dis)
        {
            std::string s;
            f.str(line);
            f.clear();
            getline(f, s, ' ');
            getline(f, s,' ');
            temp.tyd.push_back(atof( s.c_str()));
            continue;
        }

        if(read_trans_z_val)
        {
            std::string s;
            f.str(line);
            f.clear();
            getline(f, s, ' ');
            getline(f, s,' ');
            temp.tz.push_back(atof( s.c_str()));
            continue;
        }

        if(read_trans_z_dis)
        {
            std::string s;
            f.str(line);
            f.clear();
            getline(f, s, ' ');
            getline(f, s,' ');
            temp.tzd.push_back(atof(s.c_str()));
            continue;
        }


        //Rotation comes now
        if(read_rot_x_val)
        {
            std::string s;
            f.str(line);
            f.clear();
            getline(f, s, ' ');
            getline(f, s,' ');
            temp.rx.push_back(atof( s.c_str()));
            continue;
        }

        if(read_rot_x_dis)
        {
            std::string s;
            f.str(line);
            f.clear();
            getline(f, s, ' ');
            getline(f, s,' ');
            temp.rxd.push_back(atof( s.c_str()));
            continue;
        }


        if(read_rot_y_val)
        {
            std::string s;
            f.str(line);
            f.clear();
            getline(f, s, ' ');
            getline(f, s,' ');
            temp.ry.push_back(atof( s.c_str()));
            continue;
        }

        if(read_rot_y_dis)
        {
            std::string s;
            f.str(line);
            f.clear();
            getline(f, s, ' ');
            getline(f, s,' ');
            temp.ryd.push_back(atof( s.c_str()));
            continue;
        }

        if(read_rot_z_val)
        {
            std::string s;
            f.str(line);
            f.clear();
            getline(f, s, ' ');
            getline(f, s,' ');
            temp.rz.push_back(atof( s.c_str()));
            continue;
        }

        if(read_rot_z_dis)
        {
            std::string s;
            f.str(line);
            f.clear();
            getline(f, s, ' ');
            getline(f, s,' ');
            temp.rzd.push_back(atof( s.c_str()));
            continue;
        }



        //Scale comes now
        if(read_scale_x_val)
        {
            std::string s;
            f.str(line);
            f.clear();
            getline(f, s, ' ');
            getline(f, s,' ');
            temp.sx.push_back(atof( s.c_str()));
            continue;
        }

        if(read_scale_x_dis)
        {
            std::string s;
            f.str(line);
            f.clear();
            getline(f, s, ' ');
            getline(f, s,' ');
            temp.sxd.push_back(atof( s.c_str()));
            continue;
        }


        if(read_scale_y_val)
        {
            std::string s;
            f.str(line);
            f.clear();
            getline(f, s, ' ');
            getline(f, s,' ');
            temp.sy.push_back(atof( s.c_str()));
            continue;
        }

        if(read_scale_y_dis)
        {
            std::string s;
            f.str(line);
            f.clear();
            getline(f, s, ' ');
            getline(f, s,' ');
            temp.syd.push_back(atof( s.c_str()));
            continue;
        }

        if(read_scale_z_val)
        {
            std::string s;
            f.str(line);
            f.clear();
            getline(f, s, ' ');
            getline(f, s,' ');
            temp.sz.push_back(atof( s.c_str()));
            continue;
        }

        if(read_scale_z_dis)
        {
            std::string s;
            f.str(line);
            f.clear();
            getline(f, s, ' ');
            getline(f, s,' ');
            temp.szd.push_back(atof( s.c_str()));
            continue;
        }
    }

    //Checking the Animation file by printing it
    std::cout << "Printing Animation" << std::endl;

    std::cout << "--------------------------------------------"<< std::endl;
    std::cout << "Name: " << animation.animation_name << std::endl;
    for(int i =0; i < animation.joints.size(); i++)
    {
        std::cout << "--------------------------------------------"<< std::endl;
        std::cout << "Joint_name: " << animation.joints.at(i).name << std::endl;

        std::cout << "TX now: " << std::endl;
        print_vector_float(animation.joints.at(i).tx);
        std::cout << "TY now: " << std::endl;
        print_vector_float(animation.joints.at(i).ty);
        std::cout << "TZ now: " << std::endl;
        print_vector_float(animation.joints.at(i).tz);

        std::cout << "RX now: " << std::endl;
        print_vector_float(animation.joints.at(i).rx);
        std::cout << "RY now: " << std::endl;
        print_vector_float(animation.joints.at(i).ry);
        std::cout << "RZ now: " << std::endl;
        print_vector_float(animation.joints.at(i).rz);

        std::cout << "SX now: " << std::endl;
        print_vector_float(animation.joints.at(i).sx);
        std::cout << "SY now: " << std::endl;
        print_vector_float(animation.joints.at(i).sy);
        std::cout << "SZ now: " << std::endl;
        print_vector_float(animation.joints.at(i).sz);

        std::cout << "TXD now: " << std::endl;
        print_vector_int(animation.joints.at(i).txd);
        std::cout << "TYD now: " << std::endl;
        print_vector_int(animation.joints.at(i).tyd);
        std::cout << "TZD now: " << std::endl;
        print_vector_int(animation.joints.at(i).tzd);

        std::cout << "RXD now: " << std::endl;
        print_vector_int(animation.joints.at(i).rxd);
        std::cout << "RYD now: " << std::endl;
        print_vector_int(animation.joints.at(i).ryd);
        std::cout << "RZD now: " << std::endl;
        print_vector_int(animation.joints.at(i).rzd);


        std::cout << "SXD now: " << std::endl;
        print_vector_int(animation.joints.at(i).sxd);
        std::cout << "SYD now: " << std::endl;
        print_vector_int(animation.joints.at(i).syd);
        std::cout << "SZD now: " << std::endl;
        print_vector_int(animation.joints.at(i).szd);

    }
    std::cout << "--------------------------------------------"<< std::endl;


    return animation;
}

_Mesh ModelLoader::load_mesh(std::string filename)
{
    _Mesh m;
    std::vector<std::string> lines;
    std::vector<float> point_lines;
    std::vector<float> uv_lines;
    std::vector<float> normal_lines;
    std::vector<int> uvi;
    std::vector<int> pointi;

    std::vector<_Vector3D> point_lines_vec;
    std::vector<_Vector2D> uv_lines_vec;
    std::vector<_Vector3D> normal_lines_vec;

    std::istringstream f;
    lines = Get_Lines(filename);
    if(lines.size() < 1)
        return m;

    bool read_points = false;
    bool read_normals = false;
    bool read_uv = false;
    bool read_pointi = false;
    bool read_uvi = false;

    for(int i = 0; i < lines.size(); i++)
    {
        std::string line = lines.at(i);

        ///Copy name to the current deformer
        if(line.find("meshname: ") != std::string::npos)
        {
            std::string s;
            f.str(line);
            f.clear();

            getline(f, s, ' ');
            getline(f,m.modelname, ' ');
            continue;
        }
        if(line.find("texturename: ") != std::string::npos)
        {
            std::string s;
            f.str(line);
            f.clear();

            getline(f, s, ' ');
            getline(f, m.material.color.filename, ' ');
            continue;
        }

        ///Weight and Index read switches here
        if(line.find("<POINTS>") != std::string::npos)
        {
            read_points = true;
            continue;
        }
        if(line.find("</POINTS>") != std::string::npos)
        {
            read_points = false;
            continue;
        }
        if(line.find("<POINTI>") != std::string::npos)
        {
            read_pointi = true;
            continue;
        }
        if(line.find("</POINTI>") != std::string::npos)
        {
            read_pointi = false;
            continue;
        }

        if(line.find("<NORMAL>") != std::string::npos)
        {
            read_normals = true;
            continue;
        }
        if(line.find("</NORMAL>") != std::string::npos)
        {
            read_normals = false;
            continue;
        }

        if(line.find("<UV>") != std::string::npos)
        {
            read_uv = true;
            continue;
        }
        if(line.find("</UV>") != std::string::npos)
        {
            read_uv = false;
            continue;
        }

        if(line.find("<UVI>") != std::string::npos)
        {
            read_uvi = true;
            continue;
        }
        if(line.find("</UVI>") != std::string::npos)
        {
            read_uvi = false;
            continue;
        }

        if(read_points)
        {
            point_lines.push_back(atof(line.c_str()));
            continue;
        }
        if(read_pointi)
        {
            pointi.push_back(atoi(line.c_str()));
            continue;
        }
        if(read_uv)
        {
            uv_lines.push_back(atof(line.c_str()));
            continue;
        }
        if(read_uvi)
        {
            uvi.push_back(atoi(line.c_str()));
            continue;
        }
        if(read_normals)
        {
            normal_lines.push_back(atof(line.c_str()));
            continue;
        }
    }

    for(int i = 0; i < point_lines.size(); i+=3)
    {
        _Vector3D v;

        v.x = point_lines.at(i+0);
        v.y = point_lines.at(i+1);
        v.z = point_lines.at(i+2);

        point_lines_vec.push_back(v);
    }

    for(int i = 0; i < normal_lines.size(); i+=3)
    {
        _Vector3D v;

        v.x = normal_lines.at(i+0);
        v.y = normal_lines.at(i+1);
        v.z = normal_lines.at(i+2);

        m.normals.push_back(v);
    }
    for(int i = 0; i < uv_lines.size(); i+=2)
    {
        _Vector2D v;

        v.x = uv_lines.at(i+0);
        v.y = uv_lines.at(i+1);
        uv_lines_vec.push_back(v);
    }

    for(int i = 0; i < pointi.size(); i++)
    {
        m.points.push_back(point_lines_vec.at(pointi.at(i)));
    }

    for(int i = 0; i < uvi.size(); i++)
    {
        m.uv.push_back(uv_lines_vec.at(uvi.at(i)));
    }

    std::cout << "Point list size: " << m.points.size() << std::endl;
    std::cout << "Normals list size: " << m.normals.size() << std::endl;
    std::cout << "Uvs list size: " << m.uv.size() << std::endl;
    std::cout << "Uv Index list size: " << uvi.size() << std::endl;
    std::cout << "Point Index list size: " << pointi.size() << std::endl;

    print_vector_vec3(m.points);
    print_vector_vec3(m.normals);
    print_vector_vec2(m.uv);

    return m;
}


///Will ignore deformer and connections, only loading mesh
_Model_Static ModelLoader::load_model_static(std::string filename)
{
    _Model_Static model;
    _Mesh mesh;
    std::stringstream ss;
    std::string mesh_name;

    ss << filename << ".mesh_text";
    mesh_name = ss.str();

    mesh = load_mesh(mesh_name);
    model.material = mesh.material;
    model.name = mesh.modelname;
    model.normals = mesh.normals;
    model.points = mesh.points;
    model.uv = mesh.uv;

    return model;
}

///Will ignore deformer and connections, only loading mesh
_Model_Skinned ModelLoader::load_model_skinned(std::string filename)
{
    _Model_Skinned model;
    _Mesh mesh;
    std::vector<class _Deformer> deformers;
    _Connections con;

    std::string mesh_name;
    std::string deformer_name;
    std::string connection_name;

    mesh_name.append(filename).append(".mesh_text");
    deformer_name.append(filename).append(".deformer");
    connection_name.append(filename).append(".connections");

    mesh = load_mesh(mesh_name);
    con = load_connections(connection_name);
    deformers = load_deformers(deformer_name);


    model.material = mesh.material;
    model.name = mesh.modelname;
    model.normals = mesh.normals;
    model.points = mesh.points;
    model.uv = mesh.uv;
    model.deformers = deformers;
    model.connections = con;

    return model;
}



