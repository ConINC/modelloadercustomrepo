#ifndef MODELLOADER_H
#define MODELLOADER_H
#include <sstream>
#include <string>
#include <vector>
#include <memory>
#include <iostream>
#include <fstream>
#include <stdlib.h>

class _Vector3D
{
public:
    float x,y,z;
};
class _Vector2D
{
public:
    float x,y;
};
class _Vector4D
{
public:
    float x,y,z,w;
};



class _Texture
{
public:
    std::string filename;
    int bitsperpixel; ///usually 4 for tga, no compression
    std::vector<unsigned char> data;
    int width, height;
};

class _Material
{
public:
    std::string name;
    _Texture color;
    _Texture normal;
    _Texture specular;
};


class _Connections
{
public:
    std::vector<std::string> parents, names;
};

class _Model_Static
{
public:
    std::string name;
    _Material material;
    std::vector<class _Vector3D> points;
    std::vector<class _Vector2D> uv;
    std::vector<class _Vector3D> normals;
};

class _Model_Skinned
{
public:
    std::string name;
    _Material material;
    std::vector<class _Vector3D> points;
    std::vector<class _Vector2D> uv;
    std::vector<class _Vector3D> normals;
    std::vector<class _Deformer> deformers;
    _Connections connections;
};

class _Mesh
{
public:
    std::string modelname = "NULL";
    _Material material;
    std::vector<class _Vector3D> points;
    std::vector<class _Vector2D> uv;
    std::vector<class _Vector3D> normals;
};


///For use in the actual game, this will go out of here soon
///Only the file is looked at here, engine specific structures
///Are done somewhere else
class _Joint
{
public:
    _Vector3D t;
    _Vector3D r;
    float s;
    std::vector<float> matrix;
    std::string name, parent;
    int index, p_index, depth;
};
///----------------------------------------------------------

class _Deformer
{
public:
    std::string name;
    std::vector<float> weights;
    std::vector<int> indices;
};

///Contains transformation of single joint
///for all frames in the animation
class _Joint_fullframes
{
public:
    std::string name;
    std::vector<float> tx,ty,tz;
    std::vector<int> txd,tyd,tzd;

    std::vector<float> rx, ry, rz;
    std::vector<int> rxd, ryd, rzd;

    std::vector<float> sx, sy, sz;
    std::vector<int> sxd, syd, szd;
};


class _Animation
{
public:
    std::vector<class _Joint_fullframes> joints;
    std::string animation_name;
    int animation_index;
};

class ModelLoader
{
public:
        ModelLoader();
        virtual ~ModelLoader();
        static void print_vector_float(std::vector<float> vec);
        static void print_vector_int(std::vector<int> vec);
        static std::vector<std::string> Get_Lines(std::string filename);
        static _Connections load_connections(std::string lines);
        static std::vector<class _Deformer> load_deformers(std::string filename);
        static _Animation load_animation(std::string filename, std::string name, int index);
        static _Mesh load_mesh(std::string filename);
        static _Model_Static load_model_static(std::string filename);
        static _Model_Skinned load_model_skinned(std::string filename);
        static void print_vector_vec3(std::vector<_Vector3D> vec);
        static void print_vector_vec2(std::vector<_Vector2D> vec);
    protected:

    private:
};

#endif // MODELLOADER_H
